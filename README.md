# EPM-to-Grouper sync script

Simple Python script that syncs the contents of a table/view in an Oracle database, consisting of 1 column (NetID username), to a Grouper group.

Credentials and relevant environment info are in the ```sync.sh``` file, which is what should be executed (from Cron or however you wish to invoke it).

The Python script has a few module dependencies, which you can see in the ```import``` line in ```sync.py```