#!/bin/sh

# where the Python script is located
export PY_SCRIPT_DIR=/home/windhamg

# setup Oracle environment for Python's cx_Oracle
export ORACLE_HOME=/opt/oracle/product/10.2.0/client_1/
export LD_LIBRARY_PATH=:/opt/oracle/product/10.2.0/client_1/lib/

# setup environment variable for EDS stuff
export EDS_USER="eds app user username"
export EDS_PW="eds app user password"
export EDS_BASE_DN="dc=eds,dc=arizona,dc=edu"
export EDS_HOST="ldaps://eds.arizona.edu"

# Oracle connect parms
export OCI_CONNSTR='(DESCRIPTION=(ADDRESS_LIST=(FAILOVER=on)(LOAD_BALANCE=on)(SOURCE_ROUTE=off)(ADDRESS=(PROTOCOL=TCP)(HOST=uaz-ep-d50-vip.mosaic.arizona.edu)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=uaz-ep-d51-vip.mosaic.arizona.edu)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=uaz-hr-d50-vip.mosaic.arizona.edu)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=uaz-hr-d51-vip.mosaic.arizona.edu)(PORT=1521)))(CONNECT_DATA=(FAILOVER_MODE=(TYPE=session)(METHOD=basic))(SERVER=dedicated)(SERVICE_NAME=ep_gen.mosaic.arizona.edu)))'
export OCI_USER="eds oci user username"
export OCI_PW="eds oci user password"

# Grouper WS stuff
export GROUPER_WS_HOST="grouper.arizona.edu"
export GROUPER_BASE_PATH="/grouper-ws/servicesRest/json/v2_2_001/groups/"
export DEST_GROUP_NAME='arizona.edu:services:enterprise:uaccessanalytics:netidplus-required'

${PY_SCRIPT_DIR}/sync.py

