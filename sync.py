#!/usr/bin/python26

import os, re, base64, email, hmac, hashlib, urllib, urllib2, ldap, json, cx_Oracle
#from pprint import pprint

# EDS connection paramaters
eds_user = os.environ['EDS_USER']
eds_base_dn = os.environ['EDS_BASE_DN']
eds_bind_dn = "uid=" + eds_user + ",ou=app users," + eds_base_dn
eds_bind_pw = os.environ['EDS_PW']
eds_search_dn = "ou=people," + eds_base_dn
eds_host = os.environ['EDS_HOST']

# Grouper params
grouper_host = os.environ['GROUPER_WS_HOST']
grouper_base_path = os.environ['GROUPER_BASE_PATH']
grouper_grp_name = os.environ['DEST_GROUP_NAME']
grouper_user = eds_user
grouper_pass = eds_bind_pw

# Oracle connect parms
dsn_tns = os.environ['OCI_CONNSTR']
oci_user = os.environ['OCI_USER']
oci_pw = os.environ['OCI_PW']

# get all group members from Grouper
req = urllib2.Request("https://%s%s%s/members" % (grouper_host, grouper_base_path, grouper_grp_name), None, {'Authorization': 'Basic %s' % base64.b64encode('%s:%s' % (grouper_user, grouper_pass))})
try:
    result = urllib2.urlopen(req)
except urllib2.HTTPError as e:
    print e
    exit(1)

data = result.read()
grouper_resp = json.loads(data)
grouper_members = []
if 'wsSubjects' in grouper_resp['WsGetMembersLiteResult']:
    for rec in grouper_resp['WsGetMembersLiteResult']['wsSubjects']:
        if re.search('^\d{12}$',rec['id']) and rec['sourceId'] == "ldap":
            grouper_members.append(rec['id'])

# get group members from EPM table
try:
    conn = cx_Oracle.connect(oci_user, oci_pw, dsn_tns)
except cx_Oracle.DatabaseError as e:
    error, = e.args
    if error.code == 1017:
        print('Please check your credentials.')
        # sys.exit()?
    else:
        print('Database connection error: %s'.format(e))

c = conn.cursor()
c.execute('SELECT g_member FROM external.analytics_high_priv_accounts')
members = c.fetchall()
c.close()
conn.close()

# get UAIDs for all EPM table members
ld = ldap.initialize(eds_host)
ld.simple_bind_s(eds_bind_dn, eds_bind_pw)
epm_members = []
for rec in members:
    e = ld.search_s( "ou=people," + eds_base_dn, ldap.SCOPE_SUBTREE, '(uid=%s)' % rec[0], ['uaid'] )
    if (e):
        epm_members.append(e[0][1]['uaid'][0])
    else:
        print "*** EDS record for " + rec[0] + " not found"

# delete all members from Grouper not in EPM table
del_mems = set(grouper_members) - set(epm_members)
for m in del_mems:
    opener = urllib2.build_opener(urllib2.HTTPHandler)
    req = urllib2.Request("https://%s%s%s/members/%s" % (grouper_host, grouper_base_path, grouper_grp_name, m), None, {'Authorization': 'Basic %s' % base64.b64encode('%s:%s' % (grouper_user, grouper_pass))})
    req.get_method = lambda: 'DELETE'
    try:
        data = opener.open(req).read()
        grouper_resp = json.loads(data)
        if grouper_resp['WsDeleteMemberLiteResult']['resultMetadata']['resultCode'] != 'SUCCESS':
            print "Error deleting %s : %s" % (m, grouper_resp['WsDeleteMemberLiteResult']['resultMetadata']['resultCode'])
            exit(1)
    except urllib2.HTTPError as e:
        print e
        exit(1)

# add all missing members to Grouper
add_mems = set(epm_members) - set(grouper_members)
for m in add_mems:
    opener = urllib2.build_opener(urllib2.HTTPHandler)
    req = urllib2.Request("https://%s%s%s/members/%s" % (grouper_host, grouper_base_path, grouper_grp_name, m), None, {'Authorization': 'Basic %s' % base64.b64encode('%s:%s' % (grouper_user, grouper_pass))})
    req.get_method = lambda: 'PUT'
    try:
        data = opener.open(req).read()
        grouper_resp = json.loads(data)
        if grouper_resp['WsAddMemberLiteResult']['resultMetadata']['resultCode'] not in ('SUCCESS','SUCCESS_ALREADY_EXISTED'):
            print "Error adding %s : %s" % (m, grouper_resp['WsAddMemberLiteResult']['resultMetadata']['resultCode'])
            exit(1)
    except urllib2.HTTPError as e:
        print e
        exit(1)

